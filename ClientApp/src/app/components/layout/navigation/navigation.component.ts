import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { TranslateService } from 'src/app/share/services/translate.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  show = false;
  sidenavWidth = 0;
  isLogin = false;

  languages: any;
  lang: string;

  constructor(
    private authService: AuthenticateService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.isLogin = this.authService.isLogin();
    this.languages = this.translate.getLanguages();
    this.lang = this.translate.getLanguage();
  }

  logout() {
    this.authService.logout();
    this.isLogin = false;
  }

  changeLanguage(key: string) {
    this.translate.use(key);
  }
}
