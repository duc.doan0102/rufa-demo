import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, Form, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { SignUpModel } from 'src/app/models/signUp.model';
import { from } from 'rxjs';
import { MustMatch, ValidateAllFormFields } from 'src/app/validators/custom-validator';
import { HttpService } from 'src/app/share/services/http.services.ts/http.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss', '../authenticate.component.scss']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;
  email: FormControl;
  password: FormControl;
  confirm: FormControl;
  isCreated = false;
  process = 0;
  hide = true;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpService
  ) {
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.firstName = new FormControl('', Validators.required);
    this.lastName = new FormControl('', Validators.required);
    this.email = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);
    this.password = new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]);
    this.confirm = new FormControl('');
  }

  createForm() {
    this.signUpForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      password: this.password,
      confirm: this.confirm
    }, {
      validator: MustMatch('password', 'confirm')
    });
  }

  signUp() {
    if (this.signUpForm.valid) {
      this.http.post(`user/signup`, this.signUpForm.value).subscribe(
        result => {
          this.isCreated = true;
        }, error => {
          console.log(error);
        }
      );
    } else {
      ValidateAllFormFields(this.signUpForm);
    }
  }
}
