import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ValidateAllFormFields } from 'src/app/validators/custom-validator';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthenticateService } from 'src/app/services/authenticate.service';
import { TranslateService } from 'src/app/share/services/translate.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss', '../authenticate.component.scss']
})
export class SignInComponent implements OnInit {

  signInForm: FormGroup;
  email: FormControl;
  password: FormControl;
  loginFail = false;
  confirmationToken: string;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticateService: AuthenticateService,
    private translate: TranslateService
  ) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParam => {
        this.confirmationToken = queryParam.confirmationToken;
        if (this.confirmationToken !== undefined) {
          this.authenticateService.verify(this.confirmationToken).subscribe(
            result => {
              localStorage.setItem(environment.tokenKey, result.token);
              this.router.navigate(['']);
            }, error => {
            }
          );
        }
      }
    );
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern('[^ @]*@[^ @]*'),
    ]);
    this.password = new FormControl('', [
      Validators.required,
    ]);
  }

  createForm() {
    this.signInForm = this.formBuilder.group({
      email: this.email,
      password: this.password,
    });
  }

  signIn() {
    if (this.signInForm.valid) {
      this.authenticateService.login(this.signInForm.value).subscribe(
        result => {
          localStorage.setItem(environment.tokenKey, result.token);
          this.router.navigate(['']);
        }, error => {
          this.loginFail = true;
        }
      );
    } else {
      ValidateAllFormFields(this.signInForm);
    }
  }

}
