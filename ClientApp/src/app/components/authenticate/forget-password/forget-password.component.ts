import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ValidateAllFormFields } from 'src/app/validators/custom-validator';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss', '../authenticate.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  recoveryForm: FormGroup;
  email: FormControl;
  password: FormControl;
  loginFail = false;
  confirmationToken: string;
  constructor(
    private formBuilder: FormBuilder,
  ) {
  }

  ngOnInit() {

    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.email = new FormControl('', [
      Validators.required,
      Validators.pattern('[^ @]*@[^ @]*'),
    ]);
    this.password = new FormControl('', [
      Validators.required,
    ]);
  }

  createForm() {
    this.recoveryForm = this.formBuilder.group({
      email: this.email,
      password: this.password,
    });
  }
  recovery() {

  }

}
