import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'src/app/share/services/translate.service';
import { Language, LANGUAGE } from 'src/assets/i18n/lang';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss']
})
export class AuthenticateComponent implements OnInit {

  languages: any;
  lang: string;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.languages = this.translate.getLanguages();
    this.lang = this.translate.getLanguage();
  }

  changeLanguage(key: string) {
    this.translate.use(key);
  }
}
