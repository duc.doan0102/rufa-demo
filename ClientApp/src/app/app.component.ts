import { Component } from '@angular/core';
import { TranslateService } from './share/services/translate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // title = 'ClientApp';

  constructor() {
  }
}
