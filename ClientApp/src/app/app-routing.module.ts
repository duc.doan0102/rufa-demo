import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './components/authenticate/sign-in/sign-in.component';
import { SignUpComponent } from './components/authenticate/sign-up/sign-up.component';
import { ForgetPasswordComponent } from './components/authenticate/forget-password/forget-password.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { AuthenticateComponent } from './components/authenticate/authenticate.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  }, {
    path: 'auth', component: AuthenticateComponent, children: [
      { path: 'signin', component: SignInComponent },
      { path: 'forgetpassword', component: ForgetPasswordComponent },
      { path: 'signup', component: SignUpComponent },
    ]
  }, {
    path: '', component: LayoutComponent, children: [
      {
        path: 'home', component: HomeComponent
      }, {
        path: 'shop',
        loadChildren: () => import('./modules/shop/shop.module').then(mod => mod.ShopModule),
      }, {
        path: 'products',
        loadChildren: () => import('./modules/product/product.module').then(mod => mod.ProductModule),
      }, {
        path: 'categories',
        loadChildren: () => import('./modules/category/category.module').then(mod => mod.CategoryModule),
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
