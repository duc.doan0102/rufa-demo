import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category/category.component';
import { CategoryService } from './share/category.service';
import { AuthHttpService } from 'src/app/share/services/http.services.ts/auth-http.service';
import { MaterialModule } from 'src/app/material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CategoryComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    MaterialModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    CategoryService,
    AuthHttpService
  ]
})
export class CategoryModule { }
