import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/modules/category/share/category.model';
import { CategoryService } from '../share/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categories: Category[] = [];
  cloneCategories: Category[] = [];
  category: Category = new Category();

  constructor(
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe(
      result => {
        this.categories = result;
        this.categories.forEach(category => {
          category.isEdit = false;
          this.cloneCategories.push({ ...category });
        });
      }
    );
  }

  save(category) {
    this.categoryService.update(category).subscribe(
      result => {
      }
    );
  }

  discard(categoryId) {
    const index = this.categories.map(category => category.id).indexOf(categoryId);
    this.categories[index] = this.cloneCategories[index];
  }

  create() {
    this.categoryService.create(this.category).subscribe(
      result => {
        const createdCategory = result.category;
        this.categories.unshift(createdCategory);
      }
    );
  }
}
