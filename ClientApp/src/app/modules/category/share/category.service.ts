import { AuthHttpService } from 'src/app/share/services/http.services.ts/auth-http.service';
import { Observable } from 'rxjs';
import { Category } from 'src/app/modules/category/share/category.model';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CategoryService {
    constructor(
        private http: AuthHttpService,
    ) {

    }

    getCategories(): Observable<Category[]> {
        return this.http.get(`category`);
    }

    update(category: Category): Observable<any> {
        return this.http.put(`category/${category.id}`, category);
    }

    create(category: Category): Observable<any> {
        return this.http.post(`category`, category);
    }
}
