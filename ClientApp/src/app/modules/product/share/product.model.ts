import { Category } from '../../category/share/category.model';

export class Product {
    id: number;
    name: string;
    imageURL: string;
    price: number;
    star: number;
    status: boolean;
    unit: string;
    category: Category = new Category();
    categoryId: number;
    shortDescription: string;
    longDescription: string;
    calories: number;
    carbohydrate: number;
    fiber: number;
    sugar: number;
    fat: number;
    protein: number;
}
