import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthHttpService } from 'src/app/share/services/http.services.ts/auth-http.service';
import { Product } from './product.model';

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    constructor(
        private http: AuthHttpService
    ) { }

    getProducts(): Observable<Product[]> {
        return this.http.get(`product`);
    }

    getProduct(id: number): Observable<Product> {
        return this.http.get(`product/${id}`);
    }

    update(product: Product): Observable<any> {
        return this.http.put(`product/${product.id}`, product);
    }

    create(product: Product): Observable<any> {
        return this.http.post(`product`, product);
    }
}
