import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductService } from './share/product.service';
import { MaterialModule } from 'src/app/material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AuthHttpService } from 'src/app/share/services/http.services.ts/auth-http.service';
import { TranslateService } from 'src/app/share/services/translate.service';


@NgModule({
  declarations: [
    ProductListComponent,
    ProductFormComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    MaterialModule,
    NgbModule,
    FormsModule,
  ],
  providers: [
    ProductService,
    AuthHttpService
  ]
})
export class ProductModule { }
