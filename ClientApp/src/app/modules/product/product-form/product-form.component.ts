import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { ActionStatus } from 'src/app/share/const/action-status';
import { Unit } from 'src/app/share/const/unit';
import { Category } from 'src/app/modules/category/share/category.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploaderComponent } from 'src/app/share/components/uploader/uploader.component';
import { MatDialog } from '@angular/material/dialog';
import { ProductService } from '../share/product.service';
import { CategoryService } from '../../category/share/category.service';
import { Product } from '../share/product.model';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;
  actionStatus = ActionStatus;
  units = Unit;
  state = ActionStatus.view;
  product: Product = new Product();
  categories: Category[] = [];
  productId: number;
  productQuantity = 1;
  downloadURL;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private categoryService: CategoryService,
    private modalService: NgbModal,
    public dialog: MatDialog
  ) {
    this.route.params.subscribe(
      params => {
        this.state = (params.mode === 'create') ? ActionStatus.create : ActionStatus.view;
        this.productId = params.id || null;
      }
    );
  }

  ngOnInit() {
    if (this.productId) {
      this.productService.getProduct(this.productId).subscribe(
        result => {
          this.product = result;
          this.product.categoryId = this.product.category.id;
        }
      );
    } else {
      this.product = new Product();
      this.product.imageURL = `assets/image.svg`;
    }
    this.categoryService.getCategories().subscribe(
      result => {
        this.categories = result;
      }
    );
  }

  save() {
    this.productService.update(this.product).subscribe(
      result => {
        this.product.category = this.categories[this.categories.map(p => p.id).indexOf(this.product.categoryId)];
      }
    );
  }

  discard() {

  }

  create() {
    this.product.imageURL = 'url';
    this.productService.create(this.product).subscribe(
      result => {
      }
    );
  }

  uploadImage() {
    const dialogRef = this.dialog.open(UploaderComponent, {
      width: '60%',
      disableClose: true,
      data: { downloadURL: this.downloadURL }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.downloadURL = result;
        this.product.imageURL = this.downloadURL;
      }
    });
  }
}
