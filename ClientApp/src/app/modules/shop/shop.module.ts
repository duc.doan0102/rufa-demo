import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShopComponent } from './shop/shop.component';
import { ShopRoutingModule } from './shop-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthHttpService } from 'src/app/share/services/http.services.ts/auth-http.service';
import { ProductService } from '../product/share/product.service';
import { ScrollingModule } from '@angular/cdk/scrolling';


@NgModule({
  declarations: [
    ShopComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ShopRoutingModule,
    MaterialModule,
    NgbModule,
    ScrollingModule
  ],
  providers: [
    AuthHttpService,
    ProductService
  ]
})
export class ShopModule { }
