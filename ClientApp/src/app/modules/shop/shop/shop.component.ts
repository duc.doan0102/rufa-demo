import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product/share/product.service';
import { MatDialog } from '@angular/material/dialog';
import { ProductFormComponent } from '../../product/product-form/product-form.component';
import { ActionStatus } from 'src/app/share/const/action-status';
import { CategoryService } from '../../category/share/category.service';
import { Category } from '../../category/share/category.model';
import { Product } from '../../product/share/product.model';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  max = 100;
  min = 0;
  value = 0;
  step = 10;
  products: Product[] = [];
  categories: Category[] = [];

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(
      result => {
        this.products = result;
      }
    );
    this.categoryService.getCategories().subscribe(
      result => {
        this.categories = result;
      }
    );
  }

  view(product: Product) {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      width: '60%',
    });
    dialogRef.componentInstance.productId = product.id;
    dialogRef.componentInstance.state = ActionStatus.view;
  }

}
