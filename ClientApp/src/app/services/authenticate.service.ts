import { Injectable, isDevMode } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from '../share/services/http.services.ts/http.service';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthenticateService {
    constructor(
        private http: HttpService
    ) { }

    login(formData: any): Observable<any> {
        return this.http.post(`user/signin`, formData);
    }

    verify(confirmationToken: any): Observable<any> {
        return this.http.get(`user/verify?confirmationToken=${confirmationToken}`);
    }

    isLogin() {
        if (localStorage.getItem(environment.tokenKey)) {
            return true;
        }
        return false;
    }

    logout() {
        if (localStorage.getItem(environment.tokenKey)) {
            localStorage.removeItem(environment.tokenKey);
        }
    }
}
