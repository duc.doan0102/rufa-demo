export const ActionStatus = {
    view: 'VIEW',
    edit: 'EDIT',
    create: 'CREATE',
};
