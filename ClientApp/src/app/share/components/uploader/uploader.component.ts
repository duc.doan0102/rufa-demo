import { Component, OnInit, Inject } from '@angular/core';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { tap, finalize } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent implements OnInit {

  isHovering: boolean;
  files: File[] = [];

  task: AngularFireUploadTask;
  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadURL;

  constructor(
    private storage: AngularFireStorage,
    private db: AngularFirestore,
    // public activeModal: NgbActiveModal
    public dialogRef: MatDialogRef<UploaderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

  }
  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }
  ngOnInit() {
    this.downloadURL = null;
  }

  returnURL(data) {
    console.log(data.url);
  }

  startUpload(files: FileList) {
    if (files.length > 1 || files.length < 0) {
      alert('Please only drag your desired profile photo.');
      return;
    }
    const file = files[0];
    const path = `${Date.now()}_${file.name}`;
    const ref = this.storage.ref(path);
    this.task = this.storage.upload(path, file);
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges().pipe(
      finalize(async () => {
        this.downloadURL = await ref.getDownloadURL().toPromise();
        this.db.collection('files').add({ downloadURL: this.downloadURL, path });
        console.log(`done`);
      }),
    );
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

  uploadAgain() {
    this.downloadURL = undefined;
    this.percentage = undefined;
  }

  close() {
    this.dialogRef.close();
  }
}
