import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ForgetPasswordComponent } from './components/authenticate/forget-password/forget-password.component';
import { SignInComponent } from './components/authenticate/sign-in/sign-in.component';
import { SignUpComponent } from './components/authenticate/sign-up/sign-up.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './share/services/http.services.ts/http.service';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { NavigationComponent } from './components/layout/navigation/navigation.component';
import { AuthenticateComponent } from './components/authenticate/authenticate.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatNativeDateModule } from '@angular/material/core';
import { MaterialModule } from './material.module';
import { TranslatePipe } from './share/pipes/translate.pipe';
import { TranslateService } from './share/services/translate.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { DropzoneDirective } from './share/directives/dropzone.directive';
import { UploaderComponent } from './share/components/uploader/uploader.component';
import { environment } from 'src/environments/environment';
import { ProductModule } from './modules/product/product.module';
import { CategoryModule } from './modules/category/category.module';

export function setupTranslateFactory(service: TranslateService) {
  return () => {
    service.use(service.getLanguage());
  };
}

const MAIN_MODULE = [
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  NgbModule,
  HttpClientModule,
  BrowserAnimationsModule,
];

const FIREBASE_MODULE = [
  AngularFireModule.initializeApp(environment.config),
  AngularFirestoreModule,
  AngularFireAuthModule,
  AngularFireStorageModule
];

const COMPONENT = [
  ForgetPasswordComponent,
  SignInComponent,
  SignUpComponent,
  HomeComponent,
  LayoutComponent,
  FooterComponent,
  NavigationComponent,
  AuthenticateComponent,
];

const MAIN_SERVICE = [
  HttpService,
];

const INTERNAL_SERVICE = [
  TranslateService,
  {
    provide: APP_INITIALIZER,
    useFactory: setupTranslateFactory,
    deps: [
      TranslateService
    ],
    multi: true
  }
];

const EXTERNAL_SERVICE = [
];

const ENTRY_COMPONENT = [
  UploaderComponent
];

@NgModule({
  declarations: [
    AppComponent,
    COMPONENT,
    TranslatePipe,
    DropzoneDirective,
    UploaderComponent,
  ],
  imports: [
    MAIN_MODULE,
    AppRoutingModule,
    MaterialModule,
    MatNativeDateModule,
    FIREBASE_MODULE,
    ProductModule,
    CategoryModule,
  ],
  providers: [
    MAIN_SERVICE,
    INTERNAL_SERVICE,
  ],
  entryComponents: [
    ENTRY_COMPONENT
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
