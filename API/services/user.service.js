const User = require('../models').User;
const bcrypt = require('bcryptjs');
const crypto = require('crypto-random-string');

module.exports = {
    checkExistEmail(email) {
        return User.findOne({
            where: { email: email }
        });
    },
    signUp(user) {
        user.confirmationToken = crypto({ length: 20 });
        return User.create(user);
    },
    signIn(user) {
        return User.findOne({
            where: {
                email: user.email,
            }
        });
    },
    signInFail(userSignInFail) {
        userSignInFail.asscessFail = userSignInFail.asscessFail + 1;
        if (userSignInFail.asscessFail > 4) {
            userSignInFail.isLock = true;
        }
        User.update(
            userSignInFail,
            { where: { email: userSignInFail.email } }
        );
    },
    verify(token) {
        User.update({
            isLock: false
        }, {
            where: { confirmationToken: token }
        });
        return User.findOne({ where: { confirmationToken: token } });
    }
} 
