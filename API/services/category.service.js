const Category = require('../models').Category;

module.exports = {
    create(category) {
        return Category.create(category);
    },
    getCategories() {
        return Category.findAll({
            attributes: ['id', 'name']
        });
    },
    getCategoryById(id) {
        return Category.findOne({
            attributes: ['id', 'name'],
            where: { id: id }
        });
    },
    delete(id) {
        return Category
            .destroy({
                where: {
                    id: id
                }
            });
    },
    update(id, category) {
        return Category
            .update(
                category,
                { where: { id: id } }
            );
    }
};
