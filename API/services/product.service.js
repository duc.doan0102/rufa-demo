const Product = require('../models').Product;
const Category = require('../models').Category;
module.exports = {
    create(product) {
        return Product.create(product);
    },
    getProducts() {
        return Product.findAll({
            include: [{
                model: Category,
                as: 'category',
                attributes: ['id', 'name']
            }],
            attributes: ['id',
                'name',
                'longDescription',
                'shortDescription',
                'imageURL',
                'price',
                'unit',
                'star',
                'status',
                'calories',
                'carbohydrate',
                'fiber',
                'sugar',
                'protein',
                'fat'
            ]
        });
    },
    getProductById(id) {
        return Product.findOne({
            include: [{
                model: Category,
                as: 'category',
                attributes: ['id', 'name']
            }],
            attributes: ['id',
                'name',
                'longDescription',
                'shortDescription',
                'imageURL',
                'price',
                'unit',
                'star',
                'status',
                'calories',
                'carbohydrate',
                'fiber',
                'sugar',
                'protein',
                'fat'
            ],
            where: { id: id }
        });
    },
    delete(id) {
        return Product
            .destroy({
                where: {
                    id: id
                }
            });
    },
    update(id, product) {
        return Product
            .update(
                product,
                { where: { id: id } }
            );
    }
};
