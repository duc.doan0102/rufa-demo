'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      lastName: {
        type: Sequelize.STRING,
        allowNull: false
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      asscessFail: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      isLock: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      recoveryToken: {
        type: Sequelize.STRING
      },
      recoveryTokenExpired: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      confirmationToken: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};
