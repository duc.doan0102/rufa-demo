'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return Promise.all([
      queryInterface.addColumn(
        'Products',
        'shortDescription',
        {
          type: Sequelize.TEXT,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'longDescription',
        {
          type: Sequelize.TEXT,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'imageURL',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'price',
        {
          type: Sequelize.DOUBLE,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'unit',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'star',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'status',
        {
          type: Sequelize.BOOLEAN,
          allowNull: false,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'calories',
        {
          type: Sequelize.DOUBLE,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'carbohydrate',
        {
          type: Sequelize.DOUBLE,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'fiber',
        {
          type: Sequelize.DOUBLE,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'sugar',
        {
          type: Sequelize.DOUBLE,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'fat',
        {
          type: Sequelize.DOUBLE,
        }
      ),
      queryInterface.addColumn(
        'Products',
        'protein',
        {
          type: Sequelize.DOUBLE,
        }
      ),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Products', 'imageURL'),
      queryInterface.removeColumn('Products', 'price'),
      queryInterface.removeColumn('Products', 'unit'),
      queryInterface.removeColumn('Products', 'star'), 
      queryInterface.removeColumn('Products', 'status'),
      queryInterface.removeColumn('Products', 'shortDescription'),
      queryInterface.removeColumn('Products', 'longDescription'),
      queryInterface.removeColumn('Products', 'calories'),
      queryInterface.removeColumn('Products', 'carbohydrate'),
      queryInterface.removeColumn('Products', 'fiber'), 
      queryInterface.removeColumn('Products', 'sugar'),
      queryInterface.removeColumn('Products', 'fat'),
      queryInterface.removeColumn('Products', 'protein'),
    ]);
  }
};
