'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Products', [
      {
        name: 'Apple',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Apples are a sweet fleshy fruit that grow around the world.

        Believed to originate in Central Asia, there are now hundreds of varieties of apples, ranging from sweet to sour.
        
        Nutritionally speaking, apples are a relatively high-carbohydrate fruit and their most significant nutrient is vitamin C.
        
        They’re a very versatile fruit; while often eaten as a snack, they’re also used in a variety of dessert recipes.`,
        shortDescription: `Apples are a sweet fleshy fruit that grow around the world.

        Believed to originate in Central Asia, there are now hundreds of varieties of apples, ranging from sweet to sour.`,
        calories: 104,
        carbohydrate: 27.6,
        fiber: 2.8,
        sugar: 21.8,
        fat: 0.3,
        protein: 0.6,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Apricot',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Sharing some resemblances to a peach, apricots are a small fleshy fruit that contain a hard stone (technically a seed) in the middle.

        The apricot is a popular fruit with a light orange color and it is best known for its vitamin A and C content.
        
        Apricots also provide various types of polyphenols, such as catechins.
        
        Interestingly, apricots require a cold winter to grow properly, so they generally grow in countries experiencing a true four seasons.`,
        shortDescription: `Sharing some resemblances to a peach, apricots are a small fleshy fruit that contain a hard stone (technically a seed) in the middle.

        The apricot is a popular fruit with a light orange color and it is best known for its vitamin A and C content.`,
        calories: 16.8,
        carbohydrate: 3.9,
        fiber: 0.7,
        sugar: 3.2,
        fat: 0.1,
        protein: 0.5,

        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Avocado',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Avocados are an interesting fruit because they are very low in carbohydrate yet high in healthy fats.

        The fruit originated in South America, possibly in Mexico or Peru, and it was first referred to in English by the name of “crocodile pear”.
        
        One of the best things about the avocado is just how adaptable it is.
        
        For instance, you may have heard of ‘avocado toast’, a trendy breakfast at the moment. However, there are many different ways to use avocados – such as making guacamole, avocado ice-cream, chocolate mousses, and many other interesting dishes.
        
        Cold-pressed avocado oil also gives olive oil a run for its money in the ‘healthiest oil’ department; it’s a heat-stable fat that contains various protective nutrients.
        
        Avocados are extremely nutrient-dense, and they are rich in fiber, protein, vitamins, and minerals – especially potassium.`,
        shortDescription: `Avocados are an interesting fruit because they are very low in carbohydrate yet high in healthy fats.

        The fruit originated in South America, possibly in Mexico or Peru, and it was first referred to in English by the name of “crocodile pear”.`,
        calories: 322,
        carbohydrate: 17.1,
        fiber: 13.5,
        sugar: 0.2,
        fat: 29.5,
        protein: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Banana',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `The banana is a tropical fruit with a long body covered in a yellow skin.

        It’s a very common fruit and—despite needing a hot climate—it’s available in most countries.
        
        Since bananas have a very sweet taste, dessert recipes often use them.
        
        In particular, banana splits, banana milk, and banana bread are some of the most popular options.
        
        Due to their accessibility and inexpensive price, bananas are one of the most popular types of fruit in the world.`,
        shortDescription: `The banana is a tropical fruit with a long body covered in a yellow skin.

        It’s a very common fruit and—despite needing a hot climate—it’s available in most countries.
        
        Since bananas have a very sweet taste, dessert recipes often use them.`,
        calories: 105,
        carbohydrate: 27.0,
        fiber: 3.1,
        sugar: 14.4,
        fat: 0.4,
        protein: 1.3,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Blueberries',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Lauded for their purported health benefits, blueberries are a fruit that just about everyone considers healthy.

        They are certainly rich in health-protective polyphenols, and they offer a decent selection of vitamins and minerals too.
        
        With a deep blue color, they are also attractive in their appearance.
        
        On a positive note, research studies demonstrate that blueberries may lower high blood pressure and improve cardiovascular risk factors (8, 9).
        
        Blueberries are somewhat low in carbohydrate too, putting them near the top of the low carb list of fruits.`,
        shortDescription: `Lauded for their purported health benefits, blueberries are a fruit that just about everyone considers healthy.

        They are certainly rich in health-protective polyphenols, and they offer a decent selection of vitamins and minerals too.
        
        With a deep blue color, they are also attractive in their appearance.`,
        calories: 57,
        carbohydrate: 14.5,
        fiber: 2.4,
        sugar: 10,
        fat: 0.3,
        protein: 0.7,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Cherry',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Cherries are one of the most popular fruit varieties, and they are technically a drupe – a fruit with a hard stone inside.

        The cherry is a small fruit with a sweet and juicy taste. There are also two main types of cherry; sour cherries and sweet cherries.
        
        These two varieties are fairly similar, but sour cherries have the slightly better nutritional profile.
        
        Sour cherries predominantly grow in Russia, Eastern Europe and Southern Asia.
        
        On the other hand, Turkey and the United States are the biggest producers of sweet cherries.
        
        Cherries are a seasonal fruit, but frozen cherries are available around the year.`,
        shortDescription: `Cherries are one of the most popular fruit varieties, and they are technically a drupe – a fruit with a hard stone inside.

        The cherry is a small fruit with a sweet and juicy taste. There are also two main types of cherry; sour cherries and sweet cherries.
        
        These two varieties are fairly similar, but sour cherries have the slightly better nutritional profile.`,
        calories: 50,
        carbohydrate: 12.2,
        fiber: 1.6,
        sugar: 8.5,
        fat: 0.3,
        protein: 1.0,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Durian',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Known for its peculiar scent, the durian is a giant-sized tropical fruit that’s native to South-East Asia.

        If you have ever seen a jackfruit, the durian shares a similar appearance—only it’s much larger in size.
        
        Durian is a popular culinary fruit and it commonly features in a variety of Thai, Malaysian and Indonesian dishes – both sweet and savory.
        
        By “peculiar” scent, what I really mean is “terrible”. There are various words to describe this smell, but perhaps the most telling one is “smells like a gym sock.”
        
        On the other hand, the taste is pretty good – sweet, creamy, and kind of nutty.
        
        Durians have an interesting nutritional profile too. In fact, they are one of the only fruits to contain a decent amount of carbohydrate and fat.`,
        shortDescription: `Known for its peculiar scent, the durian is a giant-sized tropical fruit that’s native to South-East Asia.

        If you have ever seen a jackfruit, the durian shares a similar appearance—only it’s much larger in size.
        
        Durian is a popular culinary fruit and it commonly features in a variety of Thai, Malaysian and Indonesian dishes – both sweet and savory.`,
        calories: 147,
        carbohydrate: 27.1,
        fiber: 3.8,
        sugar: 0,
        fat: 5.3,
        protein: 1.5,

        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Grape',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Grapes are one of the most prevalent types of fruit, and we can find them almost everywhere.

        Grapes are very versatile too, and they form the basis of many different wines, juices, jams, and dessert products.
        
        Not many people know this, but grapes are botanically a kind of berry. Although there are thousands of different grape varieties, they are typically classified as either white (green), red, purple, or black.
        
        The health benefits of grapes are purported to come from their high polyphenol content. However, grapes are also one of the highest-sugar fruits.
        
        As a result, red wine is often thought of as a health drink since it contains concentrated polyphenols and little sugar.`,
        shortDescription: `Grapes are one of the most prevalent types of fruit, and we can find them almost everywhere.

        Grapes are very versatile too, and they form the basis of many different wines, juices, jams, and dessert products.`,

        calories: 69,
        carbohydrate: 18.1,
        fiber: 0.9,
        sugar: 15.5,
        fat: 0.2,
        protein: 0.7,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Grapefruit',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Grapefruit is a large citrus fruit believed to originate in Asia.

        It is actually a hybrid fruit, which means that it is not an original species; it was created by the natural crossing of an orange and a pomelo.
        
        Grapefruit has a slightly bitter, sour, but a little bit sweet taste. There are also different varieties of the fruit, ranging from white to pink and red.
        
        All grapefruit are relatively high in polyphenols, notably flavonoids such as naringinen.
        
        Their vitamin and mineral profile is dominated by vitamin C, and a one cup (230g)`,
        shortDescription: `Grapefruit is a large citrus fruit believed to originate in Asia.

        It is actually a hybrid fruit, which means that it is not an original species; it was created by the natural crossing of an orange and a pomelo.`,
        calories: 96,
        carbohydrate: 24.5,
        fiber: 3.7,
        sugar: 15.8,
        fat: 0.3,
        protein: 1.8,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Guava',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Guava is a tropical fruit that grows in tropical and subtropical areas.

        Generally speaking, we can find it in Mexico and elsewhere in Central and Southern America.
        
        Guava has an interesting appearance and it has a lime-green skin with a pink fleshy center. Depending on the ripeness of the fruit, guavas may taste anything from slightly sour to sweet.
        
        Hint: as guava ripens, its green skin will slowly turn to a light yellow color. If you want sweeter tasting guava, go for one which has tinges of yellow on its skin.
        
        Nutritionally, guavas are an exceptional provider of vitamin C – just one small fruit supplies 209% of the RDA.`,
        shortDescription: `Guava is a tropical fruit that grows in tropical and subtropical areas.

        Generally speaking, we can find it in Mexico and elsewhere in Central and Southern America.`,
        calories: 37.4,
        carbohydrate: 7.9,
        fiber: 3.0,
        sugar: 4.9,
        fat: 0.5,
        protein: 1.4,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Jackfruit',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Jackfruit grows in tropical regions throughout the world, but it is believed to have originated in India.

        Jackfruit has a unique name, and the fruit has an interesting appearance too. It looks somewhat like a giant version of an avocado and it is the largest tree fruit in the world.
        
        In fact, one fruit can weigh as much as 35kg; imagine carrying that one home! This tropical fruit is said to taste incredible, with a soft and sweet peach-colored flesh waiting inside.
        
        The consensus is that the fruit has a unique taste, and it has drawn comparisons to all sorts of flavors. For instance, these comparisons range from pineapple and mango to potatoes and—surprisingly—pulled pork.`,
        shortDescription: `Jackfruit grows in tropical regions throughout the world, but it is believed to have originated in India.

        Jackfruit has a unique name, and the fruit has an interesting appearance too. It looks somewhat like a giant version of an avocado and it is the largest tree fruit in the world.`,
        calories: 155,
        carbohydrate: 39.6,
        fiber: 2.6,
        sugar: 0,
        fat: 0.5,
        protein: 2.4,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Jujube',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Jujube is otherwise known as a ‘Chinese date’ and it has been cultivated in China for centuries.

        Playing a significant role in traditional Chinese medicine, people have historically believed it to have many powerful effects including anti-bacterial, anti-inflammatory, and contraceptive properties.
        
        In terms of real scientific research, some studies suggest that it may be a viable treatment for chronic constipation (24).
        
        We can eat jujube either fresh, dried, or in many different processed products such as jujube tea.
        
        Jujube has a sweet and juicy taste when fresh, and this sweetness strengthens in their dried state.`,
        shortDescription: `Jujube is otherwise known as a ‘Chinese date’ and it has been cultivated in China for centuries.

        Playing a significant role in traditional Chinese medicine, people have historically believed it to have many powerful effects including anti-bacterial, anti-inflammatory, and contraceptive properties.`,
        calories: 79,
        carbohydrate: 20.2,
        fiber: 0,
        sugar: 0,
        fat: 0.2,
        protein: 1.2,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Kiwi',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Kiwifruit is a soft, green and juicy edible fruit.

        It has the alternate name of ‘Chinese gooseberry’ and it is one of the most famous types of fruit in the world.
        
        The fruit is relatively easy to grow and it’s easy to find in most developed countries.
        
        Kiwifruit taste slightly sour, but mostly sweet, and their green flesh and black seeds are encased in an olive-colored furry skin.
        
        Kiwis contain an impressive amount of vitamin C. Notably, one small fruit contains more than the daily RDA.`,
        shortDescription: `Kiwifruit is a soft, green and juicy edible fruit.

        It has the alternate name of ‘Chinese gooseberry’ and it is one of the most famous types of fruit in the world.`,
        calories: 46.4,
        carbohydrate: 11.1,
        fiber: 2.3,
        sugar: 6.8,
        fat: 0.4,
        protein: 0.9,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Lemon',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `If you like sour fruits, then here’s another – the lemon is a yellow citrus fruit native to Asia.

        Lemons house a tart soft flesh behind a thick yellow skin, and they are a famous culinary fruit. For one thing, thousands of recipes call for a squeeze of fresh lemon juice.
        
        Lemons also hold value for cleaning uses; the reason for this is their high concentration of citric acid.
        
        In addition, there is a wide variety of lemon-based food products, such as lemon cakes, lemon juice, lemon jam, and lemon tea.
        
        Nutritionally speaking, the major compound in lemons is vitamin C. The fruit offers the following vitamins and minerals per standard-sized fruit.`,
        shortDescription: `If you like sour fruits, then here’s another – the lemon is a yellow citrus fruit native to Asia.

        Lemons house a tart soft flesh behind a thick yellow skin, and they are a famous culinary fruit. For one thing, thousands of recipes call for a squeeze of fresh lemon juice.
        
        Lemons also hold value for cleaning uses; the reason for this is their high concentration of citric acid.`,
        calories: 25.8,
        carbohydrate: 25.4,
        fiber: 1.6,
        sugar: 1.5,
        fat: 0.2,
        protein: 0.6,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Longan Fruit',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Longan is a tropical fruit that belongs to the soapberry family.

        This botanical family also includes the slightly better-known lychee fruit and rambutan.
        
        Due to the unique look of the fruit, the nickname of ‘dragon’s eye’ is common throughout China, where the fruit may have originated.
        
        Longans are a brilliant white, with a big black seed, surrounded by an olive-brown skin.
        
        Longan fruits have a soft and smooth texture inside their skin, and they are extremely juicy. The taste is very sweet, and the fruit plays a culinary role in many Asian dishes.`,
        shortDescription: `Longan is a tropical fruit that belongs to the soapberry family.

        This botanical family also includes the slightly better-known lychee fruit and rambutan.`,
        calories: 60,
        carbohydrate: 15.1,
        fiber: 11.1,
        sugar: 0,
        fat: 0.1,
        protein: 0.3,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Lychee',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `The lychee is a tropical fruit almost identical in nature to rambutan and longan fruit.

        For example, the flesh, nutritional profile, and flavor are all fairly similar.
        
        However, the outer skin is the main difference. While longans have a light brown peel that looks like potato skin, lychees and rambutan have a textured reddish-purple outer peel.
        
        Lychee are also slightly sweeter than longan fruit, but not as sweet as rambutan.
        
        The history of the lychee goes back to the mid-11th century, and it originates in Guangdong, China. Like other types of fruit in the soapberry family, lychees provide a huge source of vitamin C.`,
        shortDescription: `The lychee is a tropical fruit almost identical in nature to rambutan and longan fruit.

        For example, the flesh, nutritional profile, and flavor are all fairly similar.
        
        However, the outer skin is the main difference. While longans have a light brown peel that looks like potato skin, lychees and rambutan have a textured reddish-purple outer peel.`,
        calories: 66,
        carbohydrate: 16.5,
        fiber: 1.3,
        sugar: 15.2,
        fat: 0.4,
        protein: 0.8,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Mango',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Sometimes referred to as the “king of fruits”, mangoes are a tropical fruit with extremely sweet and juicy flesh.

        They are native to South Asia, and they’re a common fruit in countries such as India, the Philippines, and Thailand.
        
        Mangoes contain a stone (making them a drupe) surrounded by sweet yellow flesh; this taste is slightly sweet, soft, and tangy.
        
        Generally, people eat the fruit in its raw, whole state, but there are also many smoothie and dessert recipes.
        
        The sweet taste makes sense when we consider that mangoes are one of the highest carbohydrate/sugar fruits.`,
        shortDescription: `Sometimes referred to as the “king of fruits”, mangoes are a tropical fruit with extremely sweet and juicy flesh.

        They are native to South Asia, and they’re a common fruit in countries such as India, the Philippines, and Thailand.`,
        calories: 107,
        carbohydrate: 28.1,
        fiber: 3.0,
        sugar: 24.4,
        fat: 0.4,
        protein: 0.8,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Orange',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Oranges are among the most common fruit in the world.

        The orange is a citrus fruit and, surprisingly, it is a hybrid rather than an original species. If you didn’t know about this point, then oranges are actually a hybrid of the pomelo and mandarin.
        
        Oranges have a tough outer peel that encases the soft, juicy center. Generally speaking, the fruit has a sweet and (very slight) sour taste. However, there are hundreds of orange varieties and they can vary between sweet, bitter, and sour.
        
        The sweet varieties are generally the edible kind we find in shops and in orange juice.
        
        Oranges are a relatively high-carbohydrate fruit and they provide a decent amount of vitamin C.`,
        shortDescription: `Oranges are among the most common fruit in the world.

        The orange is a citrus fruit and, surprisingly, it is a hybrid rather than an original species. If you didn’t know about this point, then oranges are actually a hybrid of the pomelo and mandarin.`,
        calories: 86.5,
        carbohydrate: 78.8,
        fiber: 4.4,
        sugar: 17.2,
        fat: 0.2,
        protein: 1.7,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Papaya',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `A tropical fruit with an exotic name; the papaya.

        Papayas originate in Central America and grow in most countries with a tropical climate.
        
        Despite previously being thought of as “exotic” they are now easy to find in most countries due to the global fruit trade.
        
        The fruit has a smooth outer skin and a soft, reddish-orange flesh inside.
        
        Papayas also have a striking appearance, and look like a cross between a giant pear and butternut squash. In terms of flavor, they are not dissimilar to mangoes but they are less sweet in nature.
        
        Similar to most brightly colored fruits, they contain a wealth of polyphenols, and they provide the following nutrients per cup (140g).`,
        shortDescription: `A tropical fruit with an exotic name; the papaya.

        Papayas originate in Central America and grow in most countries with a tropical climate.`,
        calories: 54.6,
        carbohydrate: 13.7,
        fiber: 2.5,
        sugar: 8.3,
        fat: 0.2,
        protein: 0.9,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, {
        name: 'Pineapple',
        imageURL: 'https://firebasestorage.googleapis.com/v0/b/rufa-c8588.appspot.com/o/1571390713823_image.svg?alt=media&token=27ae4a45-dc31-460f-9345-902e3f8ff902',
        price: 198.5,
        unit: 'Kilogram',
        star: 4,
        status: 1,
        categoryId: 1,
        longDescription: `Pineapples are a sweet and slightly sour tropical fruit originating from South America.

        It is now very common in tropical regions of the world such as the Philippines and the Caribbean.
        
        Pineapples have a firm yellow flesh that supplies a juicy, sweet taste.
        
        Similar to other tropical fruits, pineapples have many culinary uses. For instance, they’re a popular choice in juices, smoothies, desserts, and even pizzas.
        
        Pineapples are very rich in vitamin C and the mineral manganese.`,
        shortDescription: `Pineapples are a sweet and slightly sour tropical fruit originating from South America.

        It is now very common in tropical regions of the world such as the Philippines and the Caribbean.`,
        calories: 82.5,
        carbohydrate: 21.6,
        fiber: 2.3,
        sugar: 16.3,
        fat: 0.2,
        protein: 0.9,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Products', null, {});
  }
};
