# NDD API
NDD API is written by NodeJS (Express framework)

## Install all dependency
```bash
npm install
```

## Run server
NodeJS server will be run at localhost:8000
```bash
npm start
```

## Run unit test
Using Chai and Mocha to write unit test
```bash
npm test
```

## Database
MySql
### ORM: Sequelize

## Validator
Joi and Express validation
```bash
npm install --save express-validatior
```
