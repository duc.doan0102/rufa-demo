const joi = require('joi');

module.exports = {
    signUp: {
        body: {
            lastName: joi.string().required(),
            firstName: joi.string().required(),
            email: joi.string().email().required(),
            password: joi.string().required().min(8),
        }
    }
}
