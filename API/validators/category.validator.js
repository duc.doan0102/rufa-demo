const joi = require('joi');

module.exports = {
    create: {
        body: {
            name: joi.string().required(),
        }
    }
}
