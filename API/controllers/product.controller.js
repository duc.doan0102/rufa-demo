const express = require('express');
const productService = require('../services/product.service');

module.exports = {
    create(req, res) {
        const product = req.body;
        productService.create(product)
            .then(product => res.status(201).send(product))
            .catch(error => res.status(400).send(error));
    },
    getProducts(req, res) {
        productService.getProducts()
            .then(products => res.status(200).send(products))
            .catch(error => res.status(400).send(error));
    },
    getProductById(req, res) {
        const id = req.params.id;
        productService.getProductById(id)
            .then(product => res.status(200).send(product))
            .catch(error => res.send(400).send(error));
    },
    update(req, res) {
        const id = req.params.id;
        const product = req.body;
        productService.update(id, product)
            .then((rowsUpdate) => { res.status(200).send({ rowsUpdate: rowsUpdate[0] }) })
            .catch(error => res.status(400).send(error));
    },
    delete(req, res) {
        const id = req.params.id;
        productService.delete(id)
            .then(() => res.status(200).send({ rowsUpdate: 1 }))
            .catch(error => res.status(400).send(error));
    }
}
