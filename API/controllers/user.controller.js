const userService = require('../services/user.service');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
var SendMailConfig = require('../helper/sendMail.helper');
require('dotenv').config();

module.exports = {
    signUp(req, res) {
        const user = req.body;
        userService.checkExistEmail(user.email).then(checkEmail => {
            if (checkEmail !== null) {
                return res.status(400).send({
                    error: `The ${user.email} email address you have registered is already associated with another account.`
                });
            } else {
                userService.signUp(user)
                    .then(user => {
                        res.status(201).send({ user: user});
                        SendMailConfig.SendVerifyAccount(user.email, user.confirmationToken);
                    })
                    .catch(error => res.status(400).send({ error: error }));
            }
        })

    },
    signIn(req, res) {
        const createdUser = req.body;
        return userService.signIn(createdUser)
            .then(user => {
                if (user === null) {
                    res.status(401).send({ message: 'Email or password is incorrect' });
                } else if (user.isLock === true) {
                    res.status(401).send({ message: 'Your account is locked. Please use email to recovery this account' });
                } else {
                    var result = bcrypt.compareSync(createdUser.password, user.password);
                    if (result) {
                        let token = jwt.sign(user.toJSON(), 'QebCSy3Xj82B', { expiresIn: '30m' });
                        res.status(200).send({ token: token });
                    } else {
                        userService.signInFail(user.dataValues);
                        res.status(401).send({ message: 'Email or password is incorrect' })
                    }
                }
            })
            .catch(error => res.status(400).send({ error: error }));
    },
    verify(req, res) {
        if (req.query.confirmationToken === null) {
            res.status(401).send({ message: 'Cannot active this account' });
        } else {
            userService.verify(req.query.confirmationToken).then(user => {
                if (user === null) {
                    res.status(401).send({ message: 'Cannot active this account ' });
                } else {
                    let token = jwt.sign(user.toJSON(), 'QebCSy3Xj82B', { expiresIn: '30m' });
                    res.status(200).send({ token: token });
                }
            });
        }
    }
}
