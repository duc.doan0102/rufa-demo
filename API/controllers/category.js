const express = require('express');
const categoryService = require('../services/category.service');

module.exports = {
    create(req, res) {
        const category = req.body;
        categoryService.create(category)
            .then(category => res.status(201).send({ category: category }))
            .catch(error => res.status(400).send(error));
    },
    getCategories(req, res) {
        categoryService.getCategories()
            .then(categories => res.status(200).send(categories))
            .catch(error => res.status(400).send(error));
    },
    getCategoryById(req, res) {
        const id = req.params.id;
        categoryService.getCategoryById(id)
            .then(categories => res.status(200).send({ category: categories }))
            .catch(error => res.send(400).send(error));
    },
    update(req, res) {
        const id = req.params.id;
        const category = req.body;
        categoryService.update(id, category)
            .then((rowsUpdate) => { res.status(200).send({ rowsUpdate: rowsUpdate[0] }) })
            .catch(error => res.status(400).send(error));
    },
    delete(req, res) {
        const id = req.params.id;
        categoryService.delete(id)
            .then(() => res.status(200).send({ rowsUpdate: 1 }))
            .catch(error => res.status(400).send(error));
    }
}
