let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Products', () => {
    beforeEach((done) => {
        //Before each test we empty the database in your case
        done();
    });
    /*
     * Test the /GET route
     */
    describe('/GET products', () => {
        it('it should GET all the product', (done) => {
            chai.request(server)
                .get('/api/product')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    /*
     * Test the /POST route
     */
    describe('/POST product', () => {
        it('it should POST a product', (done) => {
            let product = {
                name: 'Orange',
                imageURL: 'url',
                price: 198.5,
                unit: 'Kilogram',
                star: 4,
                status: true,
                categoryId: 1
            };
            chai.request(server)
                .post('/api/product')
                .set({ 'Content-Type': 'application/json' })
                .send(product)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id');
                    res.body.should.have.property('name').eql(product.name);
                    res.body.should.have.property('imageURL').eql(product.imageURL);
                    res.body.should.have.property('price').eql(product.price);
                    res.body.should.have.property('unit').eql(product.unit);
                    res.body.should.have.property('star').eql(product.star);
                    res.body.should.have.property('status').eql(product.status);
                    res.body.should.have.property('categoryId').eql(product.categoryId);
                    done();
                });
        });
    });

    describe('/GET/:id product', () => {
        it('it should GET a product by the given id', (done) => {
            // TODO add a model to db then get that id to take this test
            let id = 1;
            chai.request(server)
                .get('/api/product/' + id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id').eql(id);
                    res.body.should.have.property('name');
                    res.body.should.have.property('imageURL');
                    res.body.should.have.property('price');
                    res.body.should.have.property('unit');
                    res.body.should.have.property('star');
                    res.body.should.have.property('status');
                    res.body['category'].should.be.a('object');
                    res.body['category'].should.have.property('id');
                    res.body['category'].should.have.property('name');
                    done();
                });
        });
    });

    /*
     * Test the /PUT/:id route
     */
    describe('/PUT/:id product', () => {
        it('it should UPDATE a product given the id', (done) => {
            // TODO add a model to db then get that id to take this test
            let id = 1;
            let product = {
                name: 'Orange',
                imageURL: 'url',
                price: 198.5,
                unit: 'Kilogram',
                star: 4,
                status: true,
                categoryId: 1
            };
            chai.request(server)
                .put('/api/product/' + id)
                .set({ 'Content-Type': 'application/json' })
                .send(product)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('rowsUpdate').eql(1);
                    done();
                });
        });
    });

    /*
     * Test the /DELETE/:id route
     */
    describe('/DELETE/:id product', () => {
        it('it should DELETE a product given the id', (done) => {
            // TODO add a model to db then get that id to take this test
            let id = 1;
            chai.request(server)
                .delete('/api/product/' + id)
                .set({ 'Content-Type': 'application/json' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('rowsUpdate').eql(1);
                    done();
                });
        });
    });
});
