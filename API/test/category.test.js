let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Categories', () => {
    beforeEach((done) => {
        //Before each test we empty the database in your case
        done();
    });
    /*
     * Test the /GET route
     */
    describe('/GET categories', () => {
        it('it should GET all the categories', (done) => {
            chai.request(server)
                .get('/api/category')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    /*
     * Test the /POST route
     */
    describe('/POST category', () => {
        it('it should POST a category', (done) => {
            let category = {
                name: "test",
            };
            chai.request(server)
                .post('/api/category')
                .set({ 'Content-Type': 'application/json' })
                .send(category)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.category.should.have.property('id');
                    res.body.category.should.have.property('name').eql(category.name);
                    done();
                });
        });
        it('it should not POST a book without name', (done) => {
            let category = {
                name: ""
            };
            chai.request(server)
                .post('/api/category')
                .set({ 'Content-Type': 'application/json' })
                .send(category)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.errors[0].should.have.property('field').eql("name");
                    done();
                });
        });
    });

    describe('/GET/:id category', () => {
        it('it should GET a category by the given id', (done) => {
            // TODO add a model to db then get that id to take this test
            let id = 2;
            chai.request(server)
                .get('/api/category/' + id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.category.should.have.property('id').eql(id);
                    res.body.category.should.have.property('name');
                    done();
                });
        });
    });

    /*
     * Test the /PUT/:id route
     */
    describe('/PUT/:id category', () => {
        it('it should UPDATE a category given the id', (done) => {
            // TODO add a model to db then get that id to take this test
            let id = 2;
            chai.request(server)
                .put('/api/category/' + id)
                .set({ 'Content-Type': 'application/json' })
                .send({
                    name: "SOUTH CO2",
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('rowsUpdate').eql(1);
                    done();
                });
        });
    });

    /*
     * Test the /DELETE/:id route
     */
    describe('/DELETE/:id catagory', () => {
        it('it should DELETE a category given the id', (done) => {
            // TODO add a model to db then get that id to take this test
            let id = 4;
            chai.request(server)
                .delete('/api/category/' + id)
                .set({ 'Content-Type': 'application/json' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('rowsUpdate').eql(1);
                    done();
                });
        });
    });
});
