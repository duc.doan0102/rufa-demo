'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    asscessFail: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    isLock: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    recoveryToken: {
      type: DataTypes.STRING
    },
    recoveryTokenExpired: {
      type: DataTypes.DATE
    },
    confirmationToken: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: true,
    hooks: {
      beforeCreate: (user) => {
        const salt = bcrypt.genSaltSync(10);
        user.password = bcrypt.hashSync(user.password, salt);
      }
    }
  });
  User.associate = function (models) {
    // associations can be defined here
  };

  return User;
};
