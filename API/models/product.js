'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    shortDescription:{
      type: DataTypes.TEXT,
    },
    longDescription:{
      type: DataTypes.TEXT,
    },
    imageURL: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: { min: 0 }
    },
    unit: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    star: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: { min: 0, max: 5 }
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    calories: DataTypes.DOUBLE,
    carbohydrate: DataTypes.DOUBLE,
    fiber: DataTypes.DOUBLE,
    sugar: DataTypes.DOUBLE,
    fat: DataTypes.DOUBLE,
    protein: DataTypes.DOUBLE,
  }, {
  });
  Product.associate = function (models) {
    // associations can be defined here
    Product.belongsTo(models.Category, {
      foreignKey: 'categoryId',
      onDelete: 'CASCADE',
      as: 'category'
    });
  };
  return Product;
};
