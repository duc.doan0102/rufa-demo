const nodemailer = require('nodemailer');
require('dotenv').config();
const environment = process.env;
const MailConfig = require('./mail.helper');
var gmailTransport = MailConfig.GmailTransport;
var hbs = require('nodemailer-express-handlebars');

module.exports.SendVerifyAccount = (receiver, token) => {
    MailConfig.ViewOption(gmailTransport, hbs);
    let HelperOptions = {
        from: '"Rufa technology" <rufatechnology@gmail.com>',
        to: receiver,
        subject: 'Confirmation instructions',
        template: 'signup',
        context: {
            confirmation_link: `${environment.CLIENT_HOST}/auth/signin?confirmationToken=${token}`
        }
    };
    gmailTransport.sendMail(HelperOptions, (error, info) => {
        if (error) {
            console.log(error);
        }
        
        console.log(info);
    });
}
