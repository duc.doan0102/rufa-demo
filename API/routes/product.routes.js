const express = require('express');
const productController = require('../controllers/product.controller');

const validate = require('express-validation');

const router = express.Router();

router
    .get('/', productController.getProducts)
    .post('/', productController.create)
    .get('/:id', productController.getProductById)
    .put('/:id', productController.update)
    .delete('/:id', productController.delete)

module.exports = router;
