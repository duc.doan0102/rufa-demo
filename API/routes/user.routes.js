const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
const userValidations = require('../validators/user.validator');
const validate = require('express-validation');

router
    .post('/signup', validate(userValidations.signUp), userController.signUp)
    .post('/signin', userController.signIn)
    .get('/verify', userController.verify)

module.exports = router;
