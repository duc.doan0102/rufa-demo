const express = require('express');
const categoryController = require('../controllers/category');

const categoryValidations = require('../validators/category.validator');
const validate = require('express-validation');

const router = express.Router();

router
    .get('/', categoryController.getCategories)
    .post('/', validate(categoryValidations.create), categoryController.create)
    .get('/:id', categoryController.getCategoryById)
    .put('/:id', categoryController.update)
    .delete('/:id', categoryController.delete)

module.exports = router;
