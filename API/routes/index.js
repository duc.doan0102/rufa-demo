
const express = require('express');
const categoryRouter = require('./category.routes');
const userRouter = require('./user.routes');
const productRotuer = require('./product.routes');
const router = express.Router();

router.use('/api/category', categoryRouter);
router.use('/api/user', userRouter);
router.use('/api/product', productRotuer);

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
